import 'package:flutter/material.dart';
import 'package:tugas_api/get_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  UserGet userGet = null;

  @override
  void initState() {
    super.initState();
    UserGet.connectToApiUser('1').then((value) {
      setState(() {
        userGet = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 50),
            Padding(
              padding: EdgeInsets.only(top: 30, bottom: 80),
              child: Text(
                "WELCOME",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 48,
                  // #0563BA konversi rgb => (5, 99, 186)
                  color: Color.fromRGBO(5, 99, 186, 1),
                ),
              ),
            ),
            Padding(
              padding: new EdgeInsets.all(10),
              child: CircleAvatar(
                // 128x128px konversi radius =>
                radius: 70,
                backgroundImage: NetworkImage(
                  (userGet != null)
                      ? userGet.avatar
                      : "https://i.stack.imgur.com/l60Hf.png",
                ),
              ),
            ),
            Padding(
              padding: new EdgeInsets.all(10),
              child: Text(
                (userGet != null) ? userGet.firstName : "Tidak ada data user",
                style: new TextStyle(
                  fontSize: 24,
                  // #0563BA konversi rgb => (5, 99, 186)
                  color: Color.fromRGBO(5, 99, 186, 1),
                ),
              ),
            ),
            Padding(
              padding: new EdgeInsets.all(20),
              child: Text(
                (userGet != null) ? userGet.email : "Tidak ada data user",
                style: new TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
